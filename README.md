# Sigsum logs

This project tracks the set-up and maintenance of Sigsum logs operated by Glasklar.

Besides the Sigsum log services, this includes
[witnesses](https://github.com/C2SP/C2SP/blob/main/tlog-witness.md),
[litebastion proxies](https://github.com/C2SP/C2SP/blob/main/https-bastion.md)
and monitoring of all the above.


