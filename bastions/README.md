Glasklar is operating an HTTPS Bastion service for witnesses to connect to, see
https://git.glasklar.is/sigsum/admin/litebastion for the Ansible role being used.

Its public endpoint is

    bastion.glasklar.is:443

bastion.glasklar.is runs in a VM at bastion-01.glasklarteknik.se.
