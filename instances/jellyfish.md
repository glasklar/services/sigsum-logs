# jellyfish

jellyfish is a Sigsum log meant for testing, run by Glasklar Teknik AB.
https://www.sigsum.org/services/

## Sigsum nodes

- Primary: poc.sigsum.org
- Secondary: No secondary.

## System info

The primary node runs in a KVM VM with
- 4 vCPU's
- 2 GiB RAM
- 1 GiB swap
- 10 GiB system disk (virtio file qcow2, md raid1, SSD)
- 32 GiB database storage (virtio raw LV, md raid1, SSD)

## Key handling
TODO

## Monitoring
TODO

