**DRAFT.  The current plan is to iterate on this document in "breaking" ways
until the end of April, 2025.  Feedback on what's good and bad is helpful.**

# About seasalp

This document describes configuration and operational aspects of [Glasklar
Teknik][]'s stable Sigsum log with base URL <https://seasalp.glasklar.is/>.

[Glasklar Teknik]: https://www.glasklarteknik.se/

## Changes to this document

The complete git history is available at:

  - https://git.glasklar.is/glasklar/services/sigsum-logs

Non-trivial changes are announced via the mailing list at:

  - https://lists.glasklarteknik.se/mailman3/postorius/lists/sigsum-log-operations.lists.glasklarteknik.se/

## Funding

[Glasklar Teknik][] is funded by [Mullvad VPN][].

Any changes in funding that would *eventually* affect operation of seasalp will
be [announced][] at least three years in advance.

[Mullvad VPN]: https://www.mullvad.net/
[announced]: #changes-to-this-document

## Interoperability

Seasalp implements version 1 of the following specifications:

  - https://git.glasklar.is/sigsum/project/documentation/-/blob/main/log.md
  - https://c2sp.org/tlog-cosignature
  - https://c2sp.org/tlog-witness

## Availability

Any plans to discontinue seasalp or the version 1 specifications that
seasalp complies with will be [announced][] at least one year in advance.

Operational issues are tended to during regular working hours.  This means ~8-17
CET/CEST on Monday--Friday unless it is an official [Swedish holiday][].

Planned maintenance is [announced][] at least 24h in advance.  Short disturbances like
server reboots, service restarts, and similar are not announced in advance.

Other noteworthy events are [announced][] as soon as possible.

How to report operational issues:

  - https://git.glasklar.is/glasklar/services/sigsum-logs/-/issues
  - `glasklar-services-sigsum-logs-issues (at) incoming.glasklar.is`

Where to find past operational issues:

  - https://git.glasklar.is/glasklar/services/sigsum-logs/-/blob/main/timelines/seasalp.md

[Swedish holiday]: https://en.wikipedia.org/wiki/Public_holidays_in_Sweden

## Operations

  - **Hardware:** Dedicated 1U SuperMicro server
    - Debian 12, KVM+qemu, libvirt.
  - **KVM VM:**
    - 4 vCPU's
    - 4 GiB RAM
    - 1 GiB swap
    - 20 GiB system disk (virtio file qcow2, md raid1, SSD)
    - 256 GiB database storage (virtio raw LV, md raid1, SSD)
  - **System software:** Debian 12 (Bookworm).
    - Automatic updates are enabled, running twice every 24h.
    - System is rebooting automatically into new kernel.
    - System time is synchronized with 2.debian.pool.ntp.org over SNTP.
    - Validating DNS resolver running locally.
  - **Log software:** [Trillian][], [log-go][], and [sigsum-agent][].
    - Versions are bumped manually after reading the release notes.
    - Deployment is managed by Sigsum's [ansible collection][].
  - **Key management:** Log signing key is kept in a YubiHSM2.
    - https://git.glasklar.is/sigsum/core/key-mgmt/-/blob/main/docs/key-management.md
    - https://git.glasklar.is/glasklar/trust/audit-log
  - **Rate limit:** 288 entries per 24h for each domain suffix.
  - **Physical access:** Hosting provider has physical access to the primary node.
    - Glasklar employees has access to the secondary node.
  - **Remote access:** Sysadmin team at [Glasklar Teknik][].
    - `sk-ssh-ed25519@openssh.com` keys only, no passwords.
  - **Service redundancy:** Yes.
    - On the service level, a [secondary node][] running in a separate
      data center is promoted to be the [primary node][] by a member
      of the sysadmin team with access to the passphrase for using a
      separate YubiHSM2. This is a manual process requiring physical
      access to a certain safe and data center.
    - On the log node level, each physical server has redundant power
      and all its storage in RAID 1 arrays. The primary node is
      connected to the internet with multiple upstreams.
  - **Key backup:** Yes.
    - https://git.glasklar.is/sigsum/core/key-mgmt/-/blob/main/docs/key-management.md
  - **Availability monitoring:** End-to-end [checker jobs][].

[Trillian]: https://github.com/google/trillian/
[log-go]: https://git.glasklar.is/sigsum/core/log-go
[sigsum-agent]: https://git.glasklar.is/sigsum/core/key-mgmt/#repository-overview
[ansible collection]: https://git.glasklar.is/sigsum/admin/ansible
[secondary node]: https://git.glasklar.is/sigsum/core/log-go/-/blob/468b3fdde8eb02d9574d2ac76b1853702b0077b2/doc/architecture.md#the-secondary-node
[primary node]: https://git.glasklar.is/sigsum/core/log-go/-/blob/468b3fdde8eb02d9574d2ac76b1853702b0077b2/doc/architecture.md#the-primary-node
[checker jobs]: https://git.glasklar.is/sigsum/admin/checker/-/tree/sigsum

## How to request a custom rate limit

Send an email to the non-public inbox:

  - `sigsum-logs (at) glasklarteknik.se`

Specify:

  - A brief motivation
  - The requested rate limit
  - The domain to configure this for

Expect a response within three working days.

## How to request configuration of a witness

Send an email to the non-public inbox:

  - `sigsum-logs (at) glasklarteknik.se`

Specify:

  - The witness' [verification key][]
    - The key name should be on the form of a [schema-less URL][]
  - Some "proof" that you're authorized to use the verification-key name
    - E.g., serve the verification key under a page for the same domain
  - A witness URL to use for add-checkpoint requests
  - Contact information to someone responsible for the witness' operations

Expect a response within three working days.

[verification key]: https://pkg.go.dev/golang.org/x/mod/sumdb/note#hdr-Verifying_Notes
[schema-less URL]: https://github.com/C2SP/C2SP/blob/main/signed-note.md#signatures
[origin line]: https://github.com/C2SP/C2SP/blob/main/tlog-checkpoint.md#note-text

## Configuration for trust policies

In Sigsum [policy-format][]:

    log 0ec7e16843119b120377a73913ac6acbc2d03d82432e2c36b841b09a95841f25 https://seasalp.glasklar.is

[policy-format]: https://git.glasklar.is/sigsum/core/sigsum-go/-/blob/main/doc/policy.md

## Configuration for witnesses

  - **Verification key**: `sigsum.org/v1/tree/44ad38f8226ff9bd27629a41e55df727308d0a1cd8a2c31d3170048ac1dd22a1+682b49db+AQ7H4WhDEZsSA3enOROsasvC0D2CQy4sNrhBsJqVhB8l`
  - **Expected rate:** cosignatures are collected every 10s
