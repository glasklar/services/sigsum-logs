## Verifying seasalp functionality
### Does submit work?
```
sigsum-key gen -o key
cat > policy <<EOF
log $(sigsum-key hex <<<"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA7H4WhDEZsSA3enOROsasvC0D2CQy4sNrhBsJqVhB8l sigsum key") https://seasalp.glasklar.is/
quorum none
EOF
echo foo | sigsum-submit -k key -p policy -o proof
echo foo | sigsum-verify -k key.pub -p policy proof || echo FAIL
```

### Verifying tree sizes
```
# Public log tree head
curl https://seasalp.glasklar.is/get-tree-head
# Primary's view of secondary's tree head
ssh sigsum-log-02.glasklarteknik.se "curl -s http://192.168.19.3:14785/get-secondary-tree-head"
```

### Counting cosignatures
```
curl -s https://seasalp.glasklar.is/get-tree-head | rg ^cosignature= | wc -l
```

## MariaDB datadir
Using a separate LV for the database is good.
MariaDB uses `/var/lib/mysql` for its `datadir`.
Directory `lost+found` is treated as a database.

Workaround (package mariadb-server 1:10.11.6-0+deb12u1):
```
apt install -y mariadb-server
apt remove -y mariadb-server
(cd /var/lib/mysql && rm -r aria_log* debian-*.flag ib_* ibdata1 multi-master.info mysql mysql_upgrade_info performance_schema sys)
printf "[mysqld]\ndatadir = /var/lib/mysql/db\n" > /etc/mysql/mariadb.conf.d/99-glasklar.cnf
apt install -y mariadb-server
```

## Key handling
### Log pubkey format conversion
YubiHSM provisioning gives log signing pubkey in "openssl pkey format".
```
-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEADsfhaEMRmxIDd6c5E6xqy8LQPYJDLiw2uEGwmpWEHyU=
-----END PUBLIC KEY-----
```

Here's one way of converting that to an SSH key (above blurb in pubkey.pem):
```
openssl pkey -pubin -in pubkey.pem -text -noout | sed -n '/^pub:/,$p' | grep -v '^pub:' | tr -d ' \n:' | sigsum-key hex-to-pub
```

## Secondary node logkey generation

```
(umask 0077; mkdir /dev/shm/tmp; ssh-keygen -t ed25519 -f /dev/shm/tmp/key -N "" -C seasalp:secondary)
ansible-vault encrypt_string --vault-id host@/etc/vault/$(hostname -s)_vault_secret < /dev/shm/tmp/key
```

## Wireguard tunnel for internal endpoints

Wireguard key generation:
```
apt install -y wireguard
(umask 0037; wg genkey | tee /etc/wireguard/wg-seasalp.private.key | wg pubkey > /etc/wireguard/wg-seasalp.public.key)
chgrp systemd-network /etc/wireguard
chmod 710 /etc/wireguard
chgrp systemd-network /etc/wireguard/wg-seasalp.*.key
networkctl reload
```

Example systemd-networkd config:, .netdev:
```
root@sigsum-log-02:~# cat /etc/systemd/network/30-wg-seasalp.netdev
[NetDev]
Name=wg-seasalp
Kind=wireguard

[WireGuard]
PrivateKeyFile=/etc/wireguard/wg-seasalp.private.key
ListenPort=51820

[WireGuardPeer]
PublicKey=SLdLVEcHWpEB1J3aWo/l7pfINHKv/+Xi2s5Xt+q0y3Y=
AllowedIPs=192.168.19.3/32,fd00:19::3/128
Endpoint=91.223.231.178:51820

root@sigsum-log-02:~# cat /etc/systemd/network/30-wg-seasalp.network
[Match]
Name=wg-seasalp

[Address]
Address=192.168.19.4/32
Peer=192.168.19.3/32

[Address]
Address=fd00:19::4/128
Peer=fd00:19::3/128
```

Example nftables config:
```
root@sigsum-log-02:~# cat /etc/nftables.conf.d/wireguard.conf
table inet filter {
        chain local_inp {
                iifname "enp1s0" ip saddr @limit4 udp dport 51820 accept comment "Wireguard"
                iifname "enp1s0" ip6 saddr @limit6 udp dport 51820 accept comment "Wireguard"
        }
}

root@sigsum-log-02:~# cat /etc/nftables.conf.d/sigsum.conf
table inet filter {
        chain local_inp {
                # Users
                iifname "enp1s0" tcp dport https counter accept comment "Sigsum logs"

                # seasalp internal
                iifname "wg-seasalp" ip saddr 192.168.19.3 tcp dport 14785 counter accept comment "sesalp node"
        }
}
```

## Trillian software version

```
go version -m /var/sigsum/sigsum/go/bin/trillian_log_signer | grep 'mod.*github.com/google/trillian'
go version -m /var/sigsum/sigsum/go/bin/trillian_log_server | grep 'mod.*github.com/google/trillian'
```
