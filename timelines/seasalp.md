# Timeline

| start date (UTC) | end date (UTC)   | issue                                                 | links                 |
|------------------|------------------|-------------------------------------------------------|-----------------------|
| 2025-02-09 05:02 | 2025-02-09 08:43 | seasalp unavailable for both submission and retrieval | [misc#54](https://git.glasklar.is/glasklar/services/misc/-/issues/54) [misc#38](https://git.glasklar.is/glasklar/services/misc/-/issues/38) |
